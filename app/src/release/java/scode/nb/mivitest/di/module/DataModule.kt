package scode.nb.mivitest.di.module

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import scode.nb.mivitest.data.remote.ApiWebService
import scode.nb.mivitest.utils.rx.AppSchedulerProvider
import scode.nb.mivitest.utils.rx.SchedulerProvider
import javax.inject.Singleton

@Module
internal object DataModule {

  @Singleton
  @Provides
  @JvmStatic
  fun provideRetrofit(): Retrofit = Retrofit.Builder()
      .baseUrl("https://api.github.com")
      .addConverterFactory(GsonConverterFactory.create())
      .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
      .build()

  @Singleton
  @Provides
  @JvmStatic
  fun provideGitHubService(retrofit: Retrofit): ApiWebService = retrofit.create(
      ApiWebService::class.java)

  @Provides
  @JvmStatic
  fun provideSchedulerProvider(): SchedulerProvider = AppSchedulerProvider()

}
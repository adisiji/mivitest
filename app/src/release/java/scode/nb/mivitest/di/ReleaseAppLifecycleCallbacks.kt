package scode.nb.mivitest.di

import android.app.Application
import scode.nb.mivitest.AppLifecycleCallbacks

class ReleaseAppLifecycleCallbacks : AppLifecycleCallbacks {

  override fun onCreate(application: Application) {

  }

  override fun onTerminate(application: Application) {

  }
}

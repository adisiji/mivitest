package scode.nb.mivitest.di.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import scode.nb.mivitest.data.remote.ApiWebService
import scode.nb.mivitest.utils.rx.AppSchedulerProvider
import scode.nb.mivitest.utils.rx.SchedulerProvider
import javax.inject.Singleton

@Module
internal object DataModule {

  @Singleton
  @Provides
  @JvmStatic
  fun provideSharedPref(
      application: Application): SharedPreferences = application.getSharedPreferences("MiviTest",
      Context.MODE_PRIVATE)

  @Singleton
  @Provides
  @JvmStatic
  fun provideOkHttp(): OkHttpClient {
    return OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor()
            .apply { level = HttpLoggingInterceptor.Level.BODY })
        .build()
  }

  @Singleton
  @Provides
  @JvmStatic
  fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
      .baseUrl("https://gitlab.com/mfebrianto/mivi-ios-android-test/raw/")
      .client(okHttpClient)
      .addConverterFactory(GsonConverterFactory.create())
      .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
      .build()

  @Singleton
  @Provides
  @JvmStatic
  fun provideGitHubService(retrofit: Retrofit): ApiWebService = retrofit.create(
      ApiWebService::class.java)

  @Provides
  @JvmStatic
  fun provideSchedulerProvider(): SchedulerProvider = AppSchedulerProvider()

}
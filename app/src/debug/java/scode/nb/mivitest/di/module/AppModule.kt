package scode.nb.mivitest.di.module

import dagger.Module
import dagger.Provides
import scode.nb.mivitest.AppLifecycleCallbacks
import scode.nb.mivitest.di.DebugAppLifecycleCallbacks
import javax.inject.Singleton

@Module(includes = [DataModule::class])
internal object AppModule {

  @Singleton
  @Provides
  @JvmStatic
  fun provideAppLifecycleCallbacks(): AppLifecycleCallbacks = DebugAppLifecycleCallbacks()

}
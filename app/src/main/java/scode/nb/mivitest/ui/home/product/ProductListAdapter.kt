package scode.nb.mivitest.ui.home.product

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import scode.nb.mivitest.BR
import scode.nb.mivitest.data.models.IncludedItem
import scode.nb.mivitest.databinding.ItemListProductBinding
import scode.nb.mivitest.ui.home.product.ProductListAdapter.ProductViewHolder

class ProductListAdapter(
    private val list: MutableList<IncludedItem>) : RecyclerView.Adapter<ProductViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
    val layoutInflater = LayoutInflater.from(parent.context)
    val itemProduct = ItemListProductBinding.inflate(layoutInflater, parent, false)
    return ProductViewHolder(itemProduct)
  }

  fun addItems(addedList: List<IncludedItem>) {
    list.clear()
    list.addAll(addedList)
    notifyItemRangeInserted(0, addedList.size)
  }

  override fun getItemCount(): Int = list.size

  override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
    holder.onBind(list[position])
  }

  inner class ProductViewHolder(
      private val itemBinding: ItemListProductBinding) : RecyclerView.ViewHolder(itemBinding.root) {

    fun onBind(item: IncludedItem) {
      val itemBind = ProductItemViewModel(item)
      itemBinding.setVariable(BR.viewModel, itemBind)
      //
      itemBinding.executePendingBindings()
    }

  }
}
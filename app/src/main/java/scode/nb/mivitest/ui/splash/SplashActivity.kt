package scode.nb.mivitest.ui.splash

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import scode.nb.mivitest.di.Injectable
import scode.nb.mivitest.ui.home.HomeActivity
import scode.nb.mivitest.ui.login.LoginActivity
import javax.inject.Inject

class SplashActivity : AppCompatActivity(), Injectable, SplashNavigator {

  @Inject
  lateinit var viewModelFactory: ViewModelProvider.Factory
  private lateinit var splashViewModel: SplashViewModel

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    // Making notification bar transparent
    window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

    splashViewModel = ViewModelProviders.of(this, viewModelFactory).get(SplashViewModel::class.java)
    splashViewModel.splashNavigation = this

    splashViewModel.checkLoggedUser()
  }

  override fun gotoHome() {
    val intent = Intent(this, HomeActivity::class.java)
    startActivity(intent)
    finish()
  }

  override fun gotoLogin() {
    val intent = Intent(this, LoginActivity::class.java)
    startActivity(intent)
    finish()
  }
}

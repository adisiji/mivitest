package scode.nb.mivitest.ui.products

import android.arch.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import scode.nb.mivitest.di.ViewModelKey

@Module
internal abstract class ProductModule {

  @Binds
  @IntoMap
  @ViewModelKey(ProductViewModel::class)
  abstract fun bindSubscriptionViewModel(productViewModel: ProductViewModel): ViewModel
}
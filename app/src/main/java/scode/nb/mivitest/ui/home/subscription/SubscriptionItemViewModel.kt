package scode.nb.mivitest.ui.home.subscription

import android.databinding.ObservableField
import android.databinding.ObservableInt
import scode.nb.mivitest.data.models.IncludedItem

class SubscriptionItemViewModel(includedItem: IncludedItem) {

  val subscriptionId = ObservableField<String>(includedItem.id)
  val dataBalance = ObservableInt(includedItem.attributes?.includedDataBalance ?: 0)
  val creditBalance = ObservableInt(includedItem.attributes?.includedCreditBalance ?: 0)
  val rolloverCreditBalance = ObservableInt(
      includedItem.attributes?.includedRolloverCreditBalance ?: 0)
  val rolloverDataBalance = ObservableInt(includedItem.attributes?.includedRolloverDataBalance ?: 0)
  val internationalTalkBalance = ObservableInt(
      includedItem.attributes?.includedInternationalTalkBalance ?: 0)

}
package scode.nb.mivitest.ui.splash

interface SplashNavigator {

  fun gotoHome()

  fun gotoLogin()

}
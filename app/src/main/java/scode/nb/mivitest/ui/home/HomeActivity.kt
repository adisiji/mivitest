package scode.nb.mivitest.ui.home

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import scode.nb.mivitest.BR
import scode.nb.mivitest.R
import scode.nb.mivitest.databinding.ActivityHomeBinding
import scode.nb.mivitest.di.Injectable
import scode.nb.mivitest.ui.home.product.ProductListAdapter
import scode.nb.mivitest.ui.home.subscription.SubscriptionListAdapter
import javax.inject.Inject

class HomeActivity : AppCompatActivity(), Injectable, HomeNavigator {

  @Inject
  lateinit var viewModelFactory: ViewModelProvider.Factory
  private lateinit var homeViewModel: HomeViewModel
  private lateinit var activityHomeBinding: ActivityHomeBinding
  private lateinit var adapter: SubscriptionListAdapter
  private lateinit var adapterRelProduct: ProductListAdapter
  private lateinit var adapterOtherProduct: ProductListAdapter

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    homeViewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)
    homeViewModel.homeNavigator = this

    activityHomeBinding = DataBindingUtil.setContentView(this, R.layout.activity_home)
    activityHomeBinding.setVariable(BR.viewModel, homeViewModel)
    activityHomeBinding.executePendingBindings()

    prepareRvSubs()
    prepareRvRelatedProd()
    prepareRvOtherProd()

    homeViewModel.getDataApi()
    observeLiveData()

  }

  private fun prepareRvSubs() {
    val layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
    adapter = SubscriptionListAdapter(mutableListOf())

    activityHomeBinding.rvSubscription.layoutManager = layoutManager
    activityHomeBinding.rvSubscription.itemAnimator = DefaultItemAnimator()
    activityHomeBinding.rvSubscription.adapter = adapter

  }

  private fun prepareRvRelatedProd() {
    val layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
    adapterRelProduct = ProductListAdapter(mutableListOf())

    activityHomeBinding.rvRelProduct.layoutManager = layoutManager
    activityHomeBinding.rvRelProduct.itemAnimator = DefaultItemAnimator()
    activityHomeBinding.rvRelProduct.adapter = adapterRelProduct

  }

  private fun prepareRvOtherProd() {
    val layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
    adapterOtherProduct = ProductListAdapter(mutableListOf())

    activityHomeBinding.rvOtherProduct.layoutManager = layoutManager
    activityHomeBinding.rvOtherProduct.itemAnimator = DefaultItemAnimator()
    activityHomeBinding.rvOtherProduct.adapter = adapterOtherProduct

  }

  private fun observeLiveData() {
    homeViewModel.liveSubscripton.observe(this, Observer {
      if (it != null) {
        adapter.addItems(it)
      }
    })

    homeViewModel.liveRelatedProduct.observe(this, Observer {
      if (it != null) {
        adapterRelProduct.addItems(it)
      }
    })

    homeViewModel.liveOtherProduct.observe(this, Observer {
      if (it != null) {
        adapterOtherProduct.addItems(it)
      }
    })
  }

  override fun showShortToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
  }

}

package scode.nb.mivitest.ui.home.product

import android.databinding.ObservableField
import android.databinding.ObservableInt
import scode.nb.mivitest.data.models.IncludedItem

class ProductItemViewModel(includedItem: IncludedItem) {

  val productName = ObservableField<String>(includedItem.attributes?.name)
  val includedData = ObservableField<String>(
      convertIncludeBool(includedItem.attributes?.includedData))
  val includeCredit = ObservableField<String>(
      convertIncludeBool(includedItem.attributes?.includedCredit))
  val includeInterTalk = ObservableField<String>(
      convertIncludeBool(includedItem.attributes?.includedInternationalTalk))
  val unlimitedText = ObservableField<String>(
      convertIncludeBool(includedItem.attributes?.unlimitedText))
  val unlimitedTalk = ObservableField<String>(
      convertIncludeBool(includedItem.attributes?.unlimitedTalk))
  val unlimitedInterTalk = ObservableField<String>(
      convertIncludeBool(includedItem.attributes?.unlimitedInternationalTalk))
  val unlimitedInterText = ObservableField<String>(
      convertIncludeBool(includedItem.attributes?.unlimitedInternationalText))
  val price = ObservableInt(includedItem.attributes?.price ?: 0)

  private fun convertIncludeBool(value: Boolean?): String = when (value) {
    null -> "Not Available"
    true -> "Yes"
    else -> "No"
  }

}
package scode.nb.mivitest.ui.home.subscription

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import scode.nb.mivitest.BR
import scode.nb.mivitest.data.models.IncludedItem
import scode.nb.mivitest.databinding.ItemListSubscriptionBinding
import scode.nb.mivitest.ui.home.subscription.SubscriptionListAdapter.SubscriptionViewHolder

class SubscriptionListAdapter(
    private val list: MutableList<IncludedItem>) : RecyclerView.Adapter<SubscriptionViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubscriptionViewHolder {
    val layoutInflater = LayoutInflater.from(parent.context)
    val itemListSubscriptionBinding = ItemListSubscriptionBinding.inflate(layoutInflater, parent,
        false)
    return SubscriptionViewHolder(itemListSubscriptionBinding)
  }

  fun addItems(addedList: List<IncludedItem>) {
    list.clear()
    list.addAll(addedList)
    notifyItemRangeInserted(0, addedList.size)
  }

  override fun getItemCount(): Int = list.size

  override fun onBindViewHolder(holder: SubscriptionViewHolder, position: Int) {
    holder.onBind(list[position])
  }

  inner class SubscriptionViewHolder(
      private val itemBinding: ItemListSubscriptionBinding) : RecyclerView.ViewHolder(
      itemBinding.root) {

    fun onBind(item: IncludedItem) {
      val itemBind = SubscriptionItemViewModel(item)
      itemBinding.setVariable(BR.viewModel, itemBind)
      //
      itemBinding.executePendingBindings()
    }

  }
}
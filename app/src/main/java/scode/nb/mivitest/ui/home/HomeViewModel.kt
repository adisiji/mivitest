package scode.nb.mivitest.ui.home

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import scode.nb.mivitest.data.DataRepository
import scode.nb.mivitest.data.models.CollectionResponse
import scode.nb.mivitest.data.models.IncludedItem
import scode.nb.mivitest.utils.rx.SchedulerProvider
import timber.log.Timber
import java.net.SocketTimeoutException
import javax.inject.Inject

class HomeViewModel @Inject constructor(private val dataRepository: DataRepository,
    private val schedulerProvider: SchedulerProvider) : ViewModel() {

  lateinit var homeNavigator: HomeNavigator

  val isLoading = ObservableBoolean(false)

  val accountName = ObservableField<String>()
  val dob = ObservableField<String>()
  val contact = ObservableField<String>()
  val emailAddress = ObservableField<String>()

  val liveSubscripton = MutableLiveData<MutableList<IncludedItem>>()
  val liveRelatedProduct = MutableLiveData<MutableList<IncludedItem>>()
  val liveOtherProduct = MutableLiveData<MutableList<IncludedItem>>()

  val subscriptionEmpty = ObservableBoolean(false)
  val relatedProductEmpty = ObservableBoolean(false)
  val otherProductEmpty = ObservableBoolean(false)

  fun getDataApi() {
    isLoading.set(true)
    dataRepository.getCollections()
        .subscribeOn(schedulerProvider.io())
        .observeOn(schedulerProvider.ui())
        .subscribe({
          setupView(it)
          isLoading.set(false)
        }, {
          Timber.e(it, "getDataApi: FAILED")
          if (it is SocketTimeoutException) {
            homeNavigator.showShortToast("Request Time Out")
          }
          isLoading.set(false)
        })
  }

  fun setupView(response: CollectionResponse) {
    response.data?.attributes?.let {
      accountName.set("${it.title}. ${it.firstName} ${it.lastName}")
      dob.set(it.dateOfBirth)
      contact.set(it.contactNumber)
      emailAddress.set(it.emailAddress)
    }

    val listSubs = mutableListOf<IncludedItem>()
    val tempProducts = mutableListOf<IncludedItem>()
    response.included?.forEach {
      if (it?.type ?: "" == "subscriptions") {
        it?.let { listSubs.add(it) }
      } else if (it?.type ?: "" == "products") {
        it?.let { tempProducts.add(it) }
      }
    }
    if (listSubs.isNotEmpty()) {
      liveSubscripton.value = listSubs
    } else {
      subscriptionEmpty.set(true)
    }

    val relatedProducts = mutableListOf<IncludedItem>()
    listSubs.forEach {
      it.relationships?.subscriptions?.data?.forEach { data ->
        if (data?.type ?: "" == "products") {
          tempProducts.forEach { temp ->
            if (temp.id == data?.id) {
              relatedProducts.add(temp)
              tempProducts.remove(temp)
            }
          }
        }
      }
    }

    if (relatedProducts.isNotEmpty()) {
      liveRelatedProduct.value = relatedProducts
    } else {
      relatedProductEmpty.set(true)
    }

    Timber.d(
        "setupView: otherProducts is Empty ? ${tempProducts.isEmpty()} size ${tempProducts.size}")
    if (tempProducts.isNotEmpty()) {
      liveOtherProduct.value = tempProducts
    } else {
      otherProductEmpty.set(true)
    }

  }

}
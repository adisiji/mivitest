package scode.nb.mivitest.ui.splash

import android.arch.lifecycle.ViewModel
import scode.nb.mivitest.data.DataRepository
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    private val dataRepository: DataRepository) : ViewModel() {

  lateinit var splashNavigation: SplashNavigator

  fun checkLoggedUser() {
    if (dataRepository.getAccessToken().isEmpty()) {
      splashNavigation.gotoLogin()
    } else {
      splashNavigation.gotoHome()
    }
  }
}
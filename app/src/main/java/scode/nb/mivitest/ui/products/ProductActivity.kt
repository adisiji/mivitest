package scode.nb.mivitest.ui.products

import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import scode.nb.mivitest.R
import scode.nb.mivitest.di.Injectable
import javax.inject.Inject

class ProductActivity : AppCompatActivity(), Injectable, ProductNavigator {

  @Inject
  lateinit var viewModelFactory: ViewModelProvider.Factory
  private lateinit var productViewModel: ProductViewModel

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_subscription)

  }
}

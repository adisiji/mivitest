package scode.nb.mivitest.ui.splash

import android.arch.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import scode.nb.mivitest.di.ViewModelKey

@Module
internal abstract class SplashModule {

  @Binds
  @IntoMap
  @ViewModelKey(SplashViewModel::class)
  abstract fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel

}
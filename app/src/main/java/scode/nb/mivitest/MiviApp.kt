package scode.nb.mivitest

import android.app.Activity
import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import scode.nb.mivitest.di.applyAutoInjector
import scode.nb.mivitest.di.component.DaggerAppComponent
import javax.inject.Inject

class MiviApp : Application(), HasActivityInjector {

  @Inject
  lateinit var appLifecycleCallbacks: AppLifecycleCallbacks
  @Inject
  lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

  override fun onCreate() {
    super.onCreate()
    DaggerAppComponent.builder()
        .application(this)
        .build()
        .inject(this)
    applyAutoInjector()
    appLifecycleCallbacks.onCreate(this)
  }

  override fun onTerminate() {
    appLifecycleCallbacks.onTerminate(this)
    super.onTerminate()
  }

  override fun activityInjector(): AndroidInjector<Activity> = activityDispatchingAndroidInjector
}
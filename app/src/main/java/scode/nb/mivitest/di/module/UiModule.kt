package scode.nb.mivitest.di.module

import android.arch.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import scode.nb.mivitest.di.ViewModelFactory
import scode.nb.mivitest.ui.home.HomeActivity
import scode.nb.mivitest.ui.home.HomeModule
import scode.nb.mivitest.ui.login.LoginActivity
import scode.nb.mivitest.ui.login.LoginModule
import scode.nb.mivitest.ui.products.ProductActivity
import scode.nb.mivitest.ui.products.ProductModule
import scode.nb.mivitest.ui.splash.SplashActivity
import scode.nb.mivitest.ui.splash.SplashModule

@Module
abstract class UiModule {

  @Binds
  abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

  @ContributesAndroidInjector(modules = [SplashModule::class])
  internal abstract fun contributeSplashActivity(): SplashActivity

  @ContributesAndroidInjector(modules = [LoginModule::class])
  internal abstract fun contributeLoginActivity(): LoginActivity

  @ContributesAndroidInjector(modules = [HomeModule::class])
  internal abstract fun contributeHomeActivity(): HomeActivity

  @ContributesAndroidInjector(modules = [ProductModule::class])
  internal abstract fun contributeProductActivity(): ProductActivity

}

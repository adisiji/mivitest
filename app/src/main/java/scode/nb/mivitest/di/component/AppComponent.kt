package scode.nb.mivitest.di.component

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import scode.nb.mivitest.MiviApp
import scode.nb.mivitest.di.module.AppModule
import scode.nb.mivitest.di.module.UiModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class,
  AppModule::class,
  UiModule::class
])

interface AppComponent {

  @Component.Builder
  interface Builder {

    @BindsInstance
    fun application(application: Application): Builder

    fun build(): AppComponent

  }

  fun inject(instance: MiviApp)

}



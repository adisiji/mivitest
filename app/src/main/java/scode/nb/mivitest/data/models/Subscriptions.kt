package scode.nb.mivitest.data.models


import com.google.gson.annotations.SerializedName


data class Subscriptions(

    @SerializedName("data")
    val data: List<DataItem?>? = null,

    @SerializedName("links")
    val links: Links? = null
)
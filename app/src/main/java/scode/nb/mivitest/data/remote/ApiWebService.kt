package scode.nb.mivitest.data.remote

import io.reactivex.Observable
import retrofit2.http.GET
import scode.nb.mivitest.data.models.CollectionResponse

interface ApiWebService {

  fun login(email: String, password: String)

  @GET("master/collection.json")
  fun getCollections(): Observable<CollectionResponse>

}
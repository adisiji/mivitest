package scode.nb.mivitest.data.pref

import android.content.SharedPreferences
import javax.inject.Inject

/**
 * Created by neobyte on 10/11/17.
 */

class AppPreferencesHelper @Inject
constructor(private val mPrefs: SharedPreferences) : PreferencesHelper {

  override fun setNotFirstTime() {
    mPrefs.edit().putBoolean(PREF_KEY_FIRST_TIME, false).apply()
  }

  override fun isFirstTime(): Boolean = mPrefs.getBoolean(PREF_KEY_FIRST_TIME, true)

  override fun getAccessToken(): String {
    return mPrefs.getString(PREF_KEY_ACCESS_TOKEN, "")
  }

  override fun setAccessToken(accessToken: String) {
    mPrefs.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply()
  }

  override fun deleteAccessToken() {
    mPrefs.edit().remove(PREF_KEY_ACCESS_TOKEN).apply()
  }

  override fun clearUserPrefs() {
    mPrefs.edit().remove(PREF_USER_PROFILE).apply()
  }

  companion object {
    private const val PREF_KEY_FIRST_TIME = "FIRST_TIME_USE"
    private const val PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN"
    private const val PREF_USER_PROFILE = "KEY_USER_PROFILE"
  }

}
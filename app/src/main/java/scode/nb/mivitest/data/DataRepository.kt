package scode.nb.mivitest.data

import io.reactivex.Observable
import scode.nb.mivitest.data.models.CollectionResponse
import scode.nb.mivitest.data.pref.AppPreferencesHelper
import scode.nb.mivitest.data.pref.PreferencesHelper
import scode.nb.mivitest.data.remote.ApiWebService
import javax.inject.Inject

class DataRepository @Inject constructor(private val apiWebService: ApiWebService,
    private val appPreferencesHelper: AppPreferencesHelper) : ApiWebService, PreferencesHelper {

  override fun login(email: String, password: String) = apiWebService.login(email, password)

  override fun setNotFirstTime() = appPreferencesHelper.setNotFirstTime()

  override fun isFirstTime(): Boolean = appPreferencesHelper.isFirstTime()

  override fun getAccessToken(): String = appPreferencesHelper.getAccessToken()

  override fun setAccessToken(accessToken: String) = appPreferencesHelper.setAccessToken(
      accessToken)

  override fun deleteAccessToken() = appPreferencesHelper.deleteAccessToken()

  override fun clearUserPrefs() = appPreferencesHelper.clearUserPrefs()

  override fun getCollections(): Observable<CollectionResponse> = apiWebService.getCollections()

}
package scode.nb.mivitest.data.models

import com.google.gson.annotations.SerializedName

data class AttributesInclude(

    @SerializedName("included-rollover-credit-balance")
    val includedRolloverCreditBalance: Int? = null,

    @SerializedName("auto-renewal")
    val autoRenewal: Boolean? = null,

    @SerializedName("included-data")
    val includedData: Boolean? = null,

    @SerializedName("unlimited-international-text")
    val unlimitedInternationalText: Boolean? = null,

    @SerializedName("included-data-balance")
    val includedDataBalance: Int? = null,

    @SerializedName("msn")
    val msn: String? = null,

    @SerializedName("included-credit-balance")
    val includedCreditBalance: Int? = null,

    @SerializedName("credit-expiry")
    val creditExpiry: String? = null,

    @SerializedName("unlimited-talk")
    val unlimitedTalk: Boolean? = null,

    @SerializedName("unlimited-international-talk")
    val unlimitedInternationalTalk: Boolean? = null,

    @SerializedName("primary-subscription")
    val primarySubscription: Boolean? = null,

    @SerializedName("unlimited-text")
    val unlimitedText: Boolean? = null,

    @SerializedName("included-rollover-data-balance")
    val includedRolloverDataBalance: Int? = null,

    @SerializedName("data-usage-threshold")
    val dataUsageThreshold: Boolean? = null,

    @SerializedName("expiry-date")
    val expiryDate: String? = null,

    @SerializedName("price")
    val price: Int? = null,

    @SerializedName("name")
    val name: String? = null,

    @SerializedName("included-international-talk")
    val includedInternationalTalk: Boolean? = null,

    @SerializedName("included-international-talk-balance")
    val includedInternationalTalkBalance: Int? = null,

    @SerializedName("included-credit")
    val includedCredit: Boolean? = null,

    @SerializedName("credit")
    val credit: Int? = null

)
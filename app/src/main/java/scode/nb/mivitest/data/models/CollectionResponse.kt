package scode.nb.mivitest.data.models

import com.google.gson.annotations.SerializedName

data class CollectionResponse(

    @SerializedName("data")
    val data: Data? = null,

    @SerializedName("included")
    val included: List<IncludedItem?>? = null
)
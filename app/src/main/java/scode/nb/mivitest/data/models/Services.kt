package scode.nb.mivitest.data.models

import com.google.gson.annotations.SerializedName

data class Services(

    @SerializedName("links")
    val links: Links? = null
)
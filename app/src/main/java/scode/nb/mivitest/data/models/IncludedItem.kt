package scode.nb.mivitest.data.models

import com.google.gson.annotations.SerializedName

data class IncludedItem(

    @SerializedName("relationships")
    val relationships: Relationships? = null,

    @SerializedName("attributes")
    val attributes: AttributesInclude? = null,

    @SerializedName("links")
    val links: Links? = null,

    @SerializedName("id")
    val id: String? = null,

    @SerializedName("type")
    val type: String? = null
)
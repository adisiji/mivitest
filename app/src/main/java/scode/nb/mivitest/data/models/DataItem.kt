package scode.nb.mivitest.data.models

import com.google.gson.annotations.SerializedName

data class DataItem(

    @SerializedName("id")
    val id: String? = null,

    @SerializedName("type")
    val type: String? = null
)
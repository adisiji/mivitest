package scode.nb.mivitest.data.models

import com.google.gson.annotations.SerializedName

data class Attributes(

    @SerializedName("payment-type")
    val paymentType: String? = null,

    @SerializedName("next-billing-date")
    val nextBillingDate: Any? = null,

    @SerializedName("email-subscription-status")
    val emailSubscriptionStatus: Boolean? = null,

    @SerializedName("email-address")
    val emailAddress: String? = null,

    @SerializedName("date-of-birth")
    val dateOfBirth: String? = null,

    @SerializedName("contact-number")
    val contactNumber: String? = null,

    @SerializedName("unbilled-charges")
    val unbilledCharges: Any? = null,

    @SerializedName("title")
    val title: String? = null,

    @SerializedName("first-name")
    val firstName: String? = null,

    @SerializedName("last-name")
    val lastName: String? = null,

    @SerializedName("email-address-verified")
    val emailAddressVerified: Boolean? = null
)
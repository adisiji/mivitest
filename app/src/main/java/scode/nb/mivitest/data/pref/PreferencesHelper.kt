package scode.nb.mivitest.data.pref

/**
 * Created by neobyte on 10/11/17.
 */

interface PreferencesHelper {

  fun setNotFirstTime()

  fun isFirstTime(): Boolean

  fun getAccessToken(): String

  fun setAccessToken(accessToken: String)

  fun deleteAccessToken()

  fun clearUserPrefs()

}
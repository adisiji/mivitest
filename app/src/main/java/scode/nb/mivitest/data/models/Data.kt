package scode.nb.mivitest.data.models

import com.google.gson.annotations.SerializedName

data class Data(

    @SerializedName("relationships")
    val relationships: Relationships? = null,

    @SerializedName("attributes")
    val attributes: Attributes? = null,

    @SerializedName("links")
    val links: Links? = null,

    @SerializedName("id")
    val id: String? = null,

    @SerializedName("type")
    val type: String? = null
)
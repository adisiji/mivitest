package scode.nb.mivitest.data.models


import com.google.gson.annotations.SerializedName


data class Relationships(

    @SerializedName("subscriptions")
    val subscriptions: Subscriptions? = null
)
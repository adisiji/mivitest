package scode.nb.mivitest.utils

import android.databinding.BindingAdapter
import android.view.View

object BindingUtils {

  @JvmStatic
  @BindingAdapter("observeGone")
  fun setVisibilityGone(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.GONE
  }

}